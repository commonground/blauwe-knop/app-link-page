// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"log"
	"net/http"

	http_infra "applinkpage/http"
)

type options struct {
	Organization          string `long:"organization" env:"ORGANIZATION" description:"Organization name"`
	OrganizationLogoURL   string `long:"organization-logo-url" env:"ORGANIZATION_LOGO_URL" description:"Organization logo URL"`
	HtmlTemplateDirectory string `long:"html-template-directory" env:"HTML_TEMPLATE_DIRECTORY" default:"html/templates/" description:"The directory in which the html templates can be found"`
	AppLinkProcessUrl     string `long:"app-link-process-url" env:"APP-LINK-PROCESS-URL" default:"http://localhost:8089" description:"App link process address"`
	ListenAddress         string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8088" description:"Address for the app-link-page api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %s", err)
	}
	if cliOptions.Organization == "" {
		log.Fatalf("please specify an organization")
	}

	router := http_infra.NewRouter(cliOptions.Organization, cliOptions.OrganizationLogoURL, cliOptions.HtmlTemplateDirectory, cliOptions.AppLinkProcessUrl)

	log.Printf("start listening on %s", cliOptions.ListenAddress)
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}
