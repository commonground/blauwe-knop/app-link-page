// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func NewRouter(organization string, organizationLogoURL string, htmlTemplateDirectory string, appLinkProcessUrl string) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/auth", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, htmlTemplateDirectory)
			ctx = context.WithValue(ctx, organizationNameKey, organization)
			ctx = context.WithValue(ctx, organizationLogoURLKey, organizationLogoURL)
			ctx = context.WithValue(ctx, appLinkProcessUrlKey, appLinkProcessUrl)
			handlerHome(w, r.WithContext(ctx))
		})
	})

	return r
}

type key int

const (
	htmlTemplateDirectoryKey key = iota
	organizationNameKey      key = iota
	organizationLogoURLKey   key = iota
	appLinkProcessUrlKey     key = iota
)
