# app link page

app link page written in golang.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Start the app link page:

```sh
go run cmd/app-link-page/main.go --organization demo-org
```

Or run the app link page using modd, which wil restart the API on file changes.

```sh
modd
```

By default, the app link page will run on port `8088`.

## Deployment

Prerequisites:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/)
- deployment access to the haven cluster `azure-common-prod`
